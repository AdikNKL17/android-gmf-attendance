package com.majesthink.adik.gmfattendance.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.majesthink.adik.gmfattendance.R;
import com.majesthink.adik.gmfattendance.adapter.TimeOffAdapter;
import com.majesthink.adik.gmfattendance.model.TimeOff;

import java.util.ArrayList;
import java.util.List;

public class TimeOffActivity extends AppCompatActivity {

    Context context;
    List<TimeOff> timeOffList;
    TimeOffAdapter timeOffAdapter;
    RecyclerView timeOffRecycler;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_off);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        timeOffList = new ArrayList<>();
        timeOffAdapter = new TimeOffAdapter(this, timeOffList);

        timeOffRecycler = findViewById(R.id.recycler_time_off);
        timeOffRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        timeOffRecycler.setItemAnimator(new DefaultItemAnimator());
        timeOffRecycler.setAdapter(timeOffAdapter);

        timeOffData();
    }

    private void timeOffData() {
        TimeOff timeOff = new TimeOff(1, "Jumat, 30 November 2018", "Sakit");
        timeOffList.add(timeOff);
        timeOff = new TimeOff(2, "20 -  24 September 2018", "Cuti Istri Melahirkan");
        timeOffList.add(timeOff);
        timeOff = new TimeOff(1, "Jumat, 30 November 2018", "Sakit");
        timeOffList.add(timeOff);

    }
}
