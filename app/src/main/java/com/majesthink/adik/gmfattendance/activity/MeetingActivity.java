package com.majesthink.adik.gmfattendance.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.majesthink.adik.gmfattendance.R;
import com.majesthink.adik.gmfattendance.adapter.MeetingAdapter;
import com.majesthink.adik.gmfattendance.model.Meeting;

import java.util.ArrayList;
import java.util.List;

public class MeetingActivity extends AppCompatActivity {

    Toolbar toolbar;

    List<Meeting> meetingList;
    RecyclerView recyclerMeeting;
    MeetingAdapter meetingAdapter;

    Button buttonAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        recyclerMeeting = findViewById(R.id.recycler_meeting);
        meetingList = new ArrayList<>();
        meetingAdapter = new MeetingAdapter(this, meetingList);

        recyclerMeeting.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerMeeting.setItemAnimator(new DefaultItemAnimator());
        recyclerMeeting.setAdapter(meetingAdapter);

        buttonAdd = findViewById(R.id.button_add);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AddMeetingActivity.class);
                v.getContext().startActivity(intent);
            }
        });

        dataMeeting();
    }

    private void dataMeeting() {
        Meeting meeting = new Meeting(1, "Jumat, 30 Januari 2019","PT.  Majesthink");
        meetingList.add(meeting);
        meeting = new Meeting(1, "Kamis, 29 Januari 2019","PT.  Indah Jaya kencana");
        meetingList.add(meeting);
        meeting = new Meeting(1, "Rabu, 28 Januari 2019","PT.  Djarum Super");
        meetingList.add(meeting);
        meeting = new Meeting(1, "Jumat, 30 Januari 2019","PT.  Majesthink");
        meetingList.add(meeting);

    }
}
