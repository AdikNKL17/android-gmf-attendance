package com.majesthink.adik.gmfattendance.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.majesthink.adik.gmfattendance.R;
import com.majesthink.adik.gmfattendance.adapter.HistoryUserAdapter;
import com.majesthink.adik.gmfattendance.model.HistoryUser;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;

    List<HistoryUser> historyUserList;
    RecyclerView recyclerHistory;
    HistoryUserAdapter historyUserAdapter;

    ConstraintLayout checkInHolder, checkOutHolder;
    TextView textCheckInDate, textCheckInTime, textSuccess, textCheckOutDate, textCheckOutTime;
    ImageView imageCheckInClock, imageCheckOutClock;
    Button buttonCheckIn, buttonCheckOut, buttonMeeting, buttonTimeOff, buttonHistoryAll;

    NestedScrollView nestedScrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.main_menu);

        nestedScrollView = findViewById(R.id.nested);

        //check in section
        checkInHolder = findViewById(R.id.check_in_holder);
        buttonCheckIn = findViewById(R.id.button_check_in);
        textCheckInDate = findViewById(R.id.check_in_date);
        textCheckInTime = findViewById(R.id.check_in_time);
        textSuccess = findViewById(R.id.text_success);
        imageCheckInClock = findViewById(R.id.check_in_clock);
        buttonCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.check_in_dialog);
                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                ImageButton buttonExit;
                buttonExit = dialog.findViewById(R.id.button_exit);
                ConstraintLayout checkInGps, checkInWifi, checkInQrCode, checkInBeacon;
                checkInGps = dialog.findViewById(R.id.check_in_gps);
                checkInWifi = dialog.findViewById(R.id.check_in_wifi);
                checkInQrCode = dialog.findViewById(R.id.check_in_qr_code);
                checkInBeacon = dialog.findViewById(R.id.check_in_beacon);

                checkInGps.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(v.getContext(), GpsActivity.class);
                        v.getContext().startActivity(intent);
                    }
                });

                checkInWifi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        checkInOvertime();
                    }
                });

                checkInQrCode.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(v.getContext(), BarcodeActivity.class);
                        v.getContext().startActivity(intent);
                    }
                });

                checkInBeacon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        checkInLate();
                    }
                });


                buttonExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        checkInSuccess();
                    }
                });

                dialog.show();
            }
        });

        //check out section
        checkOutHolder = findViewById(R.id.check_out_holder);
        textCheckOutDate = findViewById(R.id.check_out_date);
        textCheckOutTime = findViewById(R.id.check_out_time);
        buttonCheckOut = findViewById(R.id.button_check_out);
        imageCheckOutClock = findViewById(R.id.check_out_clock);
        imageCheckOutClock.setColorFilter(Color.parseColor("#9B9B9B"));

        buttonCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.check_in_dialog);
                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                ImageButton buttonExit;
                buttonExit = dialog.findViewById(R.id.button_exit);
                ConstraintLayout checkInGps, checkInWifi, checkInQrCode, checkInBeacon;
                checkInGps = dialog.findViewById(R.id.check_in_gps);
                checkInWifi = dialog.findViewById(R.id.check_in_wifi);
                checkInQrCode = dialog.findViewById(R.id.check_in_qr_code);
                checkInBeacon = dialog.findViewById(R.id.check_in_beacon);

                checkInGps.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        /*checkInSuccess();*/
                        Intent intent = new Intent(v.getContext(), GpsActivity.class);
                        v.getContext().startActivity(intent);
                    }
                });

                checkInWifi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        checkInOvertime();
                    }
                });

                checkInQrCode.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        /*checkInSuccess();*/
                        Intent intent = new Intent(v.getContext(), BarcodeActivity.class);
                        v.getContext().startActivity(intent);
                    }
                });

                checkInBeacon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        checkInLate();
                    }
                });


                buttonExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        checkOutSuccess();
                    }
                });

                dialog.show();
            }
        });

        buttonMeeting = findViewById(R.id.button_meeting);
        buttonTimeOff = findViewById(R.id.button_time_off);
        buttonMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MeetingActivity.class);
                v.getContext().startActivity(intent);
            }
        });
        buttonTimeOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), TimeOffActivity.class);
                v.getContext().startActivity(intent);
            }
        });

        buttonHistoryAll = findViewById(R.id.button_history_all);
        buttonHistoryAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), HistoryActivity.class);
                v.getContext().startActivity(intent);
            }
        });

        historyUserList = new ArrayList<>();
        historyUserAdapter = new HistoryUserAdapter(this, historyUserList);

        recyclerHistory = findViewById(R.id.recycler_history);
        recyclerHistory.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerHistory.setItemAnimator(new DefaultItemAnimator());
        recyclerHistory.setAdapter(historyUserAdapter);

        dataHistory();
    }


    private void checkOutEnabled() {
        checkOutHolder.setBackground(ContextCompat.getDrawable(this, R.drawable.check_in_bg));
        textCheckOutDate.setEnabled(true);
        textCheckOutTime.setEnabled(true);
        buttonCheckOut.setEnabled(true);
        buttonCheckOut.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }

    private void checkInSuccess() {
        checkInHolder.setBackground(ContextCompat.getDrawable(this, R.drawable.status_success));
        buttonCheckIn.setVisibility(View.GONE);
        textSuccess.setVisibility(View.VISIBLE);
        textSuccess.setText("Success");
        textCheckInDate.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        textCheckInTime.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        imageCheckInClock.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
        imageCheckOutClock.setColorFilter(Color.parseColor("#161616"));
        checkOutEnabled();
    }

    private void checkInLate() {
        checkInHolder.setBackground(ContextCompat.getDrawable(this, R.drawable.status_late));
        buttonCheckIn.setVisibility(View.GONE);
        textSuccess.setVisibility(View.VISIBLE);
        textSuccess.setText("Late");
        textCheckInDate.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        textCheckInTime.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        imageCheckInClock.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
        imageCheckOutClock.setColorFilter(Color.parseColor("#161616"));
        checkOutEnabled();
    }

    private void checkInOvertime() {
        checkInHolder.setBackground(ContextCompat.getDrawable(this, R.drawable.status_overtime));
        buttonCheckIn.setVisibility(View.GONE);
        textSuccess.setVisibility(View.VISIBLE);
        textSuccess.setText("Overtime");
        textCheckInDate.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        textCheckInTime.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        imageCheckInClock.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
        imageCheckOutClock.setColorFilter(Color.parseColor("#161616"));
        checkOutEnabled();
    }

    private void checkOutSuccess() {

    }

    private void dataHistory() {
        HistoryUser historyUser = new HistoryUser(0, 1, "Jumat, 30 Januari 2019", 1);
        historyUserList.add(historyUser);
        historyUser = new HistoryUser(0, 2,  "Jumat, 30 Januari 2019", 2);
        historyUserList.add(historyUser);
        historyUser = new HistoryUser(0, 3, "Jumat, 30 Januari 2019", 3);
        historyUserList.add(historyUser);
        historyUser = new HistoryUser(0, 2, "Jumat, 30 Januari 2019", 2);
        historyUserList.add(historyUser);
        historyUser = new HistoryUser(0, 3, "Jumat, 30 Januari 2019", 1);
        historyUserList.add(historyUser);
    }
}
