package com.majesthink.adik.gmfattendance.model;

public class HistoryUser {

    private int id;
    private int method;
    private String date;
    private int status;

    public HistoryUser(){

    }

    public HistoryUser(int id, int method, String date, int status) {
        this.id = id;
        this.method = method;
        this.date = date;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMethod() {
        return method;
    }

    public void setMethod(int method) {
        this.method = method;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
