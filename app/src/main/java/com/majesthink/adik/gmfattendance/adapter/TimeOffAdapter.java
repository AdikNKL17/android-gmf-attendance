package com.majesthink.adik.gmfattendance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.majesthink.adik.gmfattendance.R;
import com.majesthink.adik.gmfattendance.model.TimeOff;

import java.util.List;

public class TimeOffAdapter extends RecyclerView.Adapter<TimeOffAdapter.TimeOffHolder> {

    Context context;
    List<TimeOff> timeOffList;

    @NonNull
    @Override
    public TimeOffHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_time_off, viewGroup, false);
        return new TimeOffHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeOffHolder timeOffHolder, int i) {

        final TimeOff timeOff = timeOffList.get(i);

        timeOffHolder.textDate.setText(timeOff.getDate());
        timeOffHolder.textDescription.setText(timeOff.getDescription());

    }

    @Override
    public int getItemCount() {
        return timeOffList.size();
    }

    public class TimeOffHolder extends RecyclerView.ViewHolder {

        TextView textDate, textDescription;

        public TimeOffHolder(@NonNull View itemView) {
            super(itemView);

            textDate = itemView.findViewById(R.id.text_date);
            textDescription = itemView.findViewById(R.id.text_description);

        }
    }

    public TimeOffAdapter (Context context, List<TimeOff> timeOffList){
        this.context = context;
        this.timeOffList = timeOffList;
    }
}
