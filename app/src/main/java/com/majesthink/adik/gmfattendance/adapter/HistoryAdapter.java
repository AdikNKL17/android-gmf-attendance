package com.majesthink.adik.gmfattendance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.majesthink.adik.gmfattendance.R;
import com.majesthink.adik.gmfattendance.model.History;
import com.majesthink.adik.gmfattendance.model.HistoryUser;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryHolder> {

    Context context;
    List<History> historyList;

    @NonNull
    @Override
    public HistoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_history, viewGroup, false);


        return new HistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryHolder historyHolder, int i) {
        final History history = historyList.get(i);

        historyHolder.textName.setText(history.getUsername());



        switch (history.getStatus()){
            case 1:
                historyHolder.textStatus.setText("Success");
                historyHolder.textStatus.setBackground(ContextCompat.getDrawable(context, R.drawable.status_success));
                break;
            case 2:
                historyHolder.textStatus.setText("Late");
                historyHolder.textStatus.setBackground(ContextCompat.getDrawable(context, R.drawable.status_late));
                break;

            case 3:
                historyHolder.textStatus.setText("Overtime");
                historyHolder.textStatus.setBackground(ContextCompat.getDrawable(context, R.drawable.status_overtime));
                break;

        }

        historyHolder.historyContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }

    public class HistoryHolder extends RecyclerView.ViewHolder {

        ConstraintLayout historyContainer;

        ImageView iconPresensi;
        TextView textName;
        TextView textStatus;

        public HistoryHolder(@NonNull View itemView) {
            super(itemView);

            historyContainer = itemView.findViewById(R.id.container);
            iconPresensi = itemView.findViewById(R.id.icon_presensi);
            textName = itemView.findViewById(R.id.text_name);
            textStatus = itemView.findViewById(R.id.text_status);

        }
    }

    public HistoryAdapter(Context context, List<History> historyList){

        this.context = context;
        this.historyList = historyList;

    }
}
