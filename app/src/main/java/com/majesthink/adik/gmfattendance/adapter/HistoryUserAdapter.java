package com.majesthink.adik.gmfattendance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.majesthink.adik.gmfattendance.R;
import com.majesthink.adik.gmfattendance.model.HistoryUser;

import java.util.List;

public class HistoryUserAdapter extends RecyclerView.Adapter<HistoryUserAdapter.HistoryHolder> {

    Context context;
    List<HistoryUser> historyUserList;

    @NonNull
    @Override
    public HistoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_history_main, viewGroup, false);


        return new HistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryHolder historyHolder, int i) {
        final HistoryUser historyUser = historyUserList.get(i);

        historyHolder.textDate.setText(historyUser.getDate());

        switch (historyUser.getMethod()){
            case 1:
                historyHolder.iconPresensi.setImageResource(R.drawable.ic_gps);
                break;
            case 2:
                historyHolder.iconPresensi.setImageResource(R.drawable.ic_wifi);
                break;
            case 3:
                historyHolder.iconPresensi.setImageResource(R.drawable.ic_qr_code);
                break;
            case 4:
                historyHolder.iconPresensi.setImageResource(R.drawable.ic_beacon);
                break;
        }

        switch (historyUser.getStatus()){
            case 1:
                historyHolder.textStatus.setText("Success");
                historyHolder.textStatus.setBackground(ContextCompat.getDrawable(context, R.drawable.status_success));
                break;
            case 2:
                historyHolder.textStatus.setText("Late");
                historyHolder.textStatus.setBackground(ContextCompat.getDrawable(context, R.drawable.status_late));
                break;

            case 3:
                historyHolder.textStatus.setText("Overtime");
                historyHolder.textStatus.setBackground(ContextCompat.getDrawable(context, R.drawable.status_overtime));
                break;

        }

        historyHolder.historyContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return historyUserList.size();
    }

    public class HistoryHolder extends RecyclerView.ViewHolder {

        ConstraintLayout historyContainer;

        ImageView iconPresensi;
        TextView textDate;
        TextView textStatus;

        public HistoryHolder(@NonNull View itemView) {
            super(itemView);

            historyContainer = itemView.findViewById(R.id.container);
            iconPresensi = itemView.findViewById(R.id.icon_presensi);
            textDate = itemView.findViewById(R.id.text_date);
            textStatus = itemView.findViewById(R.id.text_status);

        }
    }

    public HistoryUserAdapter(Context context, List<HistoryUser> historyUserList){

        this.context = context;
        this.historyUserList = historyUserList;

    }
}
