package com.majesthink.adik.gmfattendance.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.majesthink.adik.gmfattendance.R;

public class LoginActivity extends AppCompatActivity {

    EditText editUsername, editPassword;
    Button buttonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editUsername = findViewById(R.id.edit_username);
        editPassword = findViewById(R.id.edit_password);

        buttonLogin = findViewById(R.id.button_login);

        if (editUsername.isHovered()){
            buttonLogin.setVisibility(View.VISIBLE);
        }

        if (editUsername.getVisibility() == View.VISIBLE){
            buttonLogin.setText("Lanjutkan");

            buttonLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editPassword.setVisibility(View.VISIBLE);
                    editUsername.setVisibility(View.GONE);
                    buttonLogin.setText("Masuk");

                    buttonLogin.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(v.getContext(), "Clicked", Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(v.getContext(), MainActivity.class);

                            v.getContext().startActivity(intent);
                        }
                    });
                }
            });
        }else{
            buttonLogin.setText("Masuk");

            buttonLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(), "Clicled", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }
}
