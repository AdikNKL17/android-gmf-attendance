package com.majesthink.adik.gmfattendance.activity;

import android.app.TimePickerDialog;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import com.majesthink.adik.gmfattendance.R;

import java.util.Calendar;

public class AddMeetingActivity extends AppCompatActivity implements View.OnClickListener {

    ConstraintLayout meetingMulai, meetingSelesai;
    private int mYear, mMonth, mDay, mHour, mMinute;
    TextView timeMulai, timeSelesai;
    Button buttonSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_meeting);

        meetingMulai = findViewById(R.id.meeting_start);
        meetingMulai.setOnClickListener(this);
        timeMulai = findViewById(R.id.time_mulai);

        buttonSave = findViewById(R.id.button_save);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.meeting_start:
                final Calendar calendar = Calendar.getInstance();
                mHour = calendar.get(Calendar.HOUR_OF_DAY);
                mMinute = calendar.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        timeMulai.setText(hourOfDay + ":" + minute);
                        timeMulai.setEnabled(true);
                        buttonSave.setEnabled(true);
                    }
                },mHour, mMinute, true);
                timePickerDialog.show();
                break;
        }
    }
}
