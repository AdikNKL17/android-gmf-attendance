package com.majesthink.adik.gmfattendance.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.majesthink.adik.gmfattendance.R;
import com.majesthink.adik.gmfattendance.adapter.HistoryAdapter;
import com.majesthink.adik.gmfattendance.adapter.HistoryUserAdapter;
import com.majesthink.adik.gmfattendance.model.History;
import com.majesthink.adik.gmfattendance.model.HistoryUser;

import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends AppCompatActivity {

    List<History> historyList;
    RecyclerView recyclerHistory;
    HistoryAdapter historyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        historyList = new ArrayList<>();
        historyAdapter = new HistoryAdapter(this, historyList);

        recyclerHistory = findViewById(R.id.recycler_history);
        recyclerHistory.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerHistory.setItemAnimator(new DefaultItemAnimator());
        recyclerHistory.setAdapter(historyAdapter);

        dataHistory();
    }

    private void dataHistory() {
        History history = new History(0, 1, "John Doe", 1);
        historyList.add(history);
        history = new History(0, 1, "John Doe", 1);
        historyList.add(history);
        history = new History(0, 1, "John Doe", 1);
        historyList.add(history);
        history = new History(0, 1, "John Doe", 1);
        historyList.add(history);
        history = new History(0, 1, "John Doe", 1);
        historyList.add(history);
    }
}
