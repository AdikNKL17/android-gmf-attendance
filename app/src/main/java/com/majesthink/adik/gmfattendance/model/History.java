package com.majesthink.adik.gmfattendance.model;

public class History {

    private int id;
    private int method;
    private String username;
    private int status;

    public History(){

    }

    public History(int id, int method, String username, int status) {
        this.id = id;
        this.method = method;
        this.username = username;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMethod() {
        return method;
    }

    public void setMethod(int method) {
        this.method = method;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
