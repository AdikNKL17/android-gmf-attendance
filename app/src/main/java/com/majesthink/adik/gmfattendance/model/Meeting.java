package com.majesthink.adik.gmfattendance.model;

public class Meeting {

    private int id;
    private String date;
    private String company;

    public Meeting(){

    }

    public Meeting(int id, String date, String company) {
        this.id = id;
        this.date = date;
        this.company = company;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
