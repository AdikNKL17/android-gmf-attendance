package com.majesthink.adik.gmfattendance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.majesthink.adik.gmfattendance.R;
import com.majesthink.adik.gmfattendance.model.Meeting;

import java.util.List;

public class MeetingAdapter extends RecyclerView.Adapter<MeetingAdapter.MeetingHolder> {

    Context context;
    List<Meeting> meetingList;

    @NonNull
    @Override
    public MeetingHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_meeting, viewGroup, false);
        return new MeetingHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MeetingHolder meetingHolder, int i) {

        final Meeting meeting = meetingList.get(i);

        meetingHolder.textMeetingDate.setText(meeting.getDate());
        meetingHolder.textMeetingCompany.setText(meeting.getCompany());

    }

    @Override
    public int getItemCount() {
        return meetingList.size();
    }

    public class MeetingHolder extends RecyclerView.ViewHolder {

        TextView textMeetingDate, textMeetingCompany;

        public MeetingHolder(@NonNull View itemView) {
            super(itemView);

            textMeetingDate = itemView.findViewById(R.id.text_meeting_date);
            textMeetingCompany = itemView.findViewById(R.id.text_meeting_company);
        }
    }

    public MeetingAdapter(Context context, List<Meeting> meetingList){

        this.context = context;
        this.meetingList = meetingList;

    }
}
